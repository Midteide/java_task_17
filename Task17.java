package payment;

import payment.*;
import java.util.Scanner;

public class Task17{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int number = 0;

        String itemName = "banana";
        double itemPrice = 3.99;

        System.out.println("Lets create an account, please type in your name:");
        String name = input.nextLine();
        Person person = new Person(name);
        System.out.println("________________________________");
        System.out.println("Hello, " + person.name + "!");
        person.printStatus();

        System.out.println("________________________________");

        System.out.println("What would you like to buy?");
        System.out.println("1)  Banana ($3.99)");
        System.out.println("2)  Water bottle ($1.85)");
        System.out.println("3)  Magazine ($13.95)");

        

        number = input.nextInt();


        switch (number){
            case 1: itemName = "Banana";
                    itemPrice = 3.99;
                    break;
            case 2: itemName = "Water bottle";
                    itemPrice = 1.85;
                    break;
            case 3: itemName = "Magazine";
                    itemPrice = 13.95;
                    break;
            default: break;
                    
        }

        // GET PAYMENT METHOD
        System.out.println("________________________________");
        System.out.println("How would you like to pay for '" + itemName + "' priced at $" + itemPrice + "?");
        System.out.println("1)  Cash");
        System.out.println("2)  Credit Card");
        System.out.println("3)  Debit Card");

        

        number = input.nextInt();

        System.out.println("________________________________");

        switch (number){
            case 1: 
                    person.payByCash(itemPrice);
            break;
            case 2: 
                    person.creditcard.Pay(itemPrice);
            break;
            case 3: 
                    person.debitcard.Pay(itemPrice);
            break;
            default: break;
                    
        }
        System.out.println("________________________________");
        person.printStatus();
        System.out.println("________________________________");

    } 
}