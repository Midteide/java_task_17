package payment;

public class Person {
    double RoundTo2Decimals(double val) {
        double roundOff = Math.round(val * 100.0) / 100.0;
        return roundOff;
    }

    public Person(String name){
        this.creditcard = new Creditcard(40,1.25);
        this.debitcard = new Debitcard(30);
        this.cash = 50;
        this.name = name;
    }
    public void printStatus(){
        System.out.println("Your current financial status:");
        System.out.println("Name: " + this.name);
        System.out.println("Credit card balance: " + this.creditcard.balance);
        System.out.println("Debit card balance: " + this.debitcard.balance);
        System.out.println("Cash in pocket: " + this.cash);
    }

    public void payByCash(double amount){
        this.cash -= amount;
        double change= this.cash;
        
        int tenDollars=0, fiveDollars=0, oneDollars=0, fiftyCents =0, quarters=0,tenCents=0, fiveCents=0, oneCents=0;
        
        while (RoundTo2Decimals(change) >= 10) {
            change -= 10;
            tenDollars++;
        }
        while (RoundTo2Decimals(change) >= 5) {
            change -= 5;
            fiveDollars++;
        }
        while (RoundTo2Decimals(change) >= 1) {
            change -= 1;
            oneDollars++;
        }
        while (RoundTo2Decimals(change) >= 0.5) {
            change -= 0.5;
            fiftyCents++;
        }
        while (RoundTo2Decimals(change) >= .25) {
            change -= .25;
            quarters++;
        }
        while (RoundTo2Decimals(change) >= 0.1) {
            change -= 0.1;
            tenCents++;
        }
        while (RoundTo2Decimals(change) >= 0.5) {
            change -= 0.5;
            fiveCents++;
        }
        while (RoundTo2Decimals(change) >= 0.01) {
            change -= 0.01;
            oneCents++;
        }
        System.out.println("You paid $" + amount + " in cash!");
        System.out.println("Your change is:");
        if (tenDollars > 0) System.out.println("*  " + tenDollars +" ten dollar bills. " );
        if (fiveDollars > 0) System.out.println("*  " + fiveDollars + " five dollar bills. " );
        if (oneDollars > 0) System.out.println("*  " + oneDollars +" one dollar bills. " );
        if (fiftyCents > 0) System.out.println("*  " + fiftyCents+ " fifty cent coins. " );
        if (quarters > 0) System.out.println("*  " + quarters+ " quarters. " );
        if (tenCents > 0) System.out.println("*  " + tenCents+ " ten cent coins. " );
        if (fiveCents > 0) System.out.println("*  " + fiveCents+ " five cent coins. " );
        if (oneCents > 0) System.out.println("*  " + oneCents+ " one cent coins. " );
        System.out.println("");
        // System.out.println("Nchangens: " + RoundTo2Decimals(change));
    }
    public Creditcard creditcard;
    public Debitcard debitcard;
    public double cash;
    public String name;

}


