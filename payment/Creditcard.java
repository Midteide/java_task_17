package payment;

public class Creditcard extends Card {
    
    public Creditcard(double bal, double fee){
        super(bal);
        this.feePerTransaction = fee;
    }
    
    public Creditcard(){
        super(0);
        this.feePerTransaction = 1.5;
    }

    public void Pay(double amount){
        this.balance -= (amount + this.feePerTransaction);
        System.out.println("You paid a total of $" + (amount + this.feePerTransaction) + " ($" + amount + 
        " + transaction fee of $" + this.feePerTransaction + ") by credit card!");
    }

    double feePerTransaction;
    


}


