package payment;

public class Debitcard extends Card {
    
    public Debitcard(double bal){
        super(bal);
    }
    
    public Debitcard(){
        super(0);
    }
    public void Pay(double amount){
        this.balance -= amount;
        System.out.println("You paid $" + amount + " by debit card");
    }
}


